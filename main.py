import itertools
from collections import OrderedDict
from flask import Flask, render_template, json
import csv, gitlab
import pandas as pd
from create_db import app, db, Artist, Artwork, Museum, create_museums_db


def gitstats():
    stats = gitlab.api()
    return stats

#json dump for artist
@app.route('/artist/JSON')
def artistJSON():
    r = json.dumps(artists)
    return r

#json dump for artwork
@app.route('/artwork/JSON')
def artworkJSON():
    r = json.dumps(artworks)
    return r

#renders home page
@app.route('/')
def index():
        return render_template('index.html')

#renders about page
@app.route('/about/')
def about():
    stats = gitstats()
    return render_template('about.html', stats = stats)

#renders artists page
@app.route('/artist/', methods=['GET', 'POST'])
def artist():
    artists = db.session.query(Artist).all()
    return render_template('artist.html', artists=artists)

#renders arworks page
@app.route('/artwork/', methods=['GET', 'POST'])
def artwork():
    artworks = db.session.query(Artwork).all()
    return render_template('artwork.html', artworks=artworks)

#renders museums 
@app.route('/museum/', methods=['GET', 'POST'])
def museum():
    museums = db.session.query(Museum).all()
    return render_template('museum.html', museums = museums)

#renders individual artist page
@app.route('/artist/<string:artist_id>/view/', methods=['GET','POST'])
def view_artist(artist_id):
    specific_artist = db.session.query(Artist).get(artist_id)
    artwork = db.session.query(Artwork).filter(artist_id == artist_id)
    return render_template("view_artist.html", specific_artist = specific_artist, artwork = artwork)

#renders individual artwork page
@app.route('/artwork/<string:artwork_id>/view/', methods=['GET','POST'])
def view_artwork(artwork_id):
    specific_artwork = db.session.query(Artwork).get(artwork_id)
    return render_template("view_artwork.html", dictionary = specific_artwork)

#renders individual museum page
@app.route('/museum/<int:museum_id>/view/', methods=['GET','POST'])
def view_museum(museum_id):
    specific_museum = db.session.query(Museum).get(museum_id)
    return render_template("view_museum.html", dictionary = specific_museum)

#renders arworks page
@app.route('/unit/', methods=['GET', 'POST'])
def unit():
    return render_template('testCases.html')

if __name__ == '__main__':
    app.run()
