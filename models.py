from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import (assert_nullable,assert_non_nullable, assert_max_length)
from sqlalchemy.orm import sessionmaker
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:YOURPASSWORD@localhost:5432/artworks')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # to suppress a warning message
db = SQLAlchemy(app)


#defines artists
class Artist(db.Model):
    __tablename__ = 'artist'

    artist_name = db.Column(db.String(), nullable=True)
    gender = db.Column(db.String(80), nullable=True)
    artist_dates = db.Column(db.String(), nullable=True)
    nationality = db.Column(db.String(), nullable=True)
    artist_id = db.Column(db.String(80), primary_key=True)

    artworks = db.relationship('Artwork', backref='painter')

#defines artwork
class Artwork(db.Model):
    __tablename__ = 'artwork'

    artist = db.Column(db.String(), nullable=True)
    date = db.Column(db.String(), nullable=True)
    medium = db.Column(db.String(), nullable=True)
    credit = db.Column(db.String(), nullable=True)
    acquired_date = db.Column(db.String(), nullable=True)
    imageURL = db.Column(db.String(), nullable=True)
    dimensions = db.Column(db.String(), nullable=True)
    museum = db.Column(db.String(), nullable=True)
    title = db.Column(db.String(), nullable =True)
    artwork_id = db.Column(db.String(), primary_key=True)

    artist_id = db.Column(db.String(80), db.ForeignKey('artist.artist_id'))
    museum_id = db.Column(db.Integer, db.ForeignKey('museum.museum_id'))

#defines museums
class Museum(db.Model):
    __tablename__ = 'museum'

    museum = db.Column(db.String(80), nullable=True)
    location = db.Column(db.String(80), nullable=True)
    museum_id = db.Column(db.Integer, primary_key=True)

    artworks = db.relationship('Artwork', backref='owner')
    # foreign key, look at diagram
    # museum_id=db.Column(db.String(80), nullable=False)



db.drop_all()
db.create_all()

#Checking conditions
artist = Artist(artist_name='John', gender='Male', nationality='US', artist_id='00001')
db.session.add(artist)
db.session.commit()
artwork=Artwork(artist='Emily', museum='Moma', title='masterpiece', artwork_id='00002')
db.session.add(artwork)
db.session.commit()
museum=Museum(museum='moma', location='France', museum_id='00003')
db.session.add(museum)
db.session.commit()

assert_nullable(artist, 'artist_name')
assert_nullable(artist, 'gender')
assert_nullable(artist, 'nationality')
assert_nullable(artwork, 'artist')
assert_nullable(artwork, 'museum')
assert_nullable(artwork, 'title')
assert_nullable(museum, 'museum')
assert_nullable(museum, 'location')
assert_max_length(artist, 'gender', 80)
assert_max_length(artist, 'artist_id', 80)
assert_max_length(museum, 'museum', 80)
assert_max_length(museum, 'location', 80)

db.session.query(Artist).delete()
db.session.commit()
db.session.query(Artwork).delete()
db.session.commit()
db.session.query(Museum).delete()
db.session.commit()
# End of models.py




